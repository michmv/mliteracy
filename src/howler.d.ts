declare interface Sprite {
    [index: string]: [number, number]
}

declare interface ConfigHowl {
    src: [string],
    sprite?: Sprite,
    volume?: number
}

declare class Howl {
    constructor(config: ConfigHowl)
    play(sprite?: string): void
    on(event: string, func: () => void): void
}