import * as NNGI from "./NNGI"

interface ListElement {
    name: string,
    letter: string
}

let list_notes: Array<ListElement> = [
    {name: 'До', letter: 'C'}, // 0
    {name: 'Ре', letter: 'D'},
    {name: 'Ми', letter: 'E'},
    {name: 'Фа', letter: 'F'},
    {name: 'Соль', letter: 'G'},
    {name: 'Ля', letter: 'A'},
    {name: 'Си', letter: 'B'} // 6
]

let list_octaves: Array<ListElement> = [
    {name: 'Контр', letter: 'C'}, // 0
    {name: 'Большая', letter: 'G'},
    {name: 'Малая', letter: 'S'},
    {name: 'Первая', letter: '1'},
    {name: 'Вторая', letter: '2'},
    {name: 'Третья', letter: '3'} // 5
]

let soundError = new Howl({
    src: ['resources/error.mp3'],
    volume: 1
})

let sound = new Howl({
    src: ['resources/notes.mp3'],
    volume: 1,
    sprite: listNoteSound()
})

function listNoteSound(): Sprite {
    let list: Sprite = {}
    let t = 0;
    for(let n=0; n<list_octaves.length; n++) {
        for(let m=0; m<list_notes.length; m++) {
            list[list_notes[m].letter + list_octaves[n].letter] = [t, 1500]
            t = t + 1500
        }
    }
    return list
}

interface Note {
    key: number,    // 0 скрипичный ключ, 1 басовый
    note: number,   // индекс ноты [0-6]
    octave: number, // индекс октавы: [0-3]
    letter: string, // короткое название
    name: string    // полное название
}

export class App {
    all: number = 0
    correct: number = 0
    note: Note
    noteView: NoteLines
    control: ControlElements
    next_step: boolean = false
    sound: Howl = sound
    soundError: Howl = soundError
    play: boolean = false
    timeId: number = 0
    help: boolean = false

    start() {
        this.next_step = false // флаг состояния смены ноты
        if(this.play) clearTimeout(this.timeId) // сбросить автоматическую смену ноты

        this.note = this.newNote()
        this.noteView.renderNote(this.note)

        this.control.resetMessage()
        this.control.inputReset()
    }

    newNote(): Note {
        //return {key:0, note:0, octave:0, name:'!!!', letter: 'C1'}
        let note = {
            key: Math.floor(Math.random() * 2), 
            note: Math.floor(Math.random() * 6), 
            octave: Math.floor(Math.random() * 4), 
            name: '', letter: ''}
        let index_octave = note.octave
        if(note.key == 0) index_octave = note.octave + 2
        note.name = list_notes[note.note].name + ' ' + list_octaves[index_octave].name
        note.letter = list_notes[note.note].letter + list_octaves[index_octave].letter
        return note
    }
}

export class Main<App> extends NNGI.Widget<App>
{
    body: JQuery<HTMLElement> = undefined

    draw(target: JQuery<HTMLElement>) {
        this.body = $('body')
        this.resize()
        let element = $('<div>', {id: 'main'})
        target.on('click', function(){ $('#in').focus() })
        target.append(element)
        return {it: element, container: element}
    }

    resize() {
        this.body.height( $(window).height() )
    }

    event(event: NNGI.EventWidget) {
        if(event instanceof NNGI.Resize) {
            this.resize()
        }

        return event
    }
}

export class NoteLines extends NNGI.Widget<App> {
    pointer_note: string = '1'

    draw(target: JQuery<HTMLElement>) {
        this.state.noteView = this
        let element = $('<div>', {id: 'note_lines', append: [
            $('<div>', {id: 'background_help'}),
            $('<div>', {id: 'note1', class: 'note_box', append: [
                $('<div>', {class: 'lines', append: $('<div>', {class: 'lines_fon'})}),
                $('<div>', {class: 'note'})
            ]}),
            $('<div>', {id: 'note2', class: 'note_box', append: [
                $('<div>', {class: 'lines', append: $('<div>', {class: 'lines_fon'})}),
                $('<div>', {class: 'note'})
            ]})
        ]})
        let help = $('<div>', {id: 'help_note', class: 'off', append: [
                $('<div>', {id: 'octave'})
            ]})
        let box = $('<div>', {id: 'note_lines_box', append: [element, help]})
        target.append(box)
        return {it: element, container: element}
    }

    renderHelp() {
        if(this.state.help) {
            $('#help_note').css('display', 'block')
            $('#background_help').css('display', 'block')
            $('#note_lines .lines_fon').addClass('help')
            if(!this.state.next_step) this.state.control.setMessage(this.state.note.name, 2)
        } else {
            $('#help_note').css('display', 'none')
            $('#background_help').css('display', 'none')
            $('#note_lines .lines_fon').removeClass('help')
            if(!this.state.next_step) this.state.control.setMessage('', 2)
        }
    }

    renderNote(note: Note) {
        let zero: number = 0
        let more_lines_begin: number = 0
        let more_lines_height: number = 0

        if(note.key > 0) {
            zero = 676 // басовый ключ
            more_lines_begin = 495;
        } else {
            zero = 364 // скрипичный ключ
            more_lines_begin = 209;
        }

        let top = zero - note.note * 13 - note.octave * 91
        more_lines_height = top - more_lines_begin

        if(more_lines_height >= 0) {
            more_lines_height = more_lines_height + 13 + 3
        } else {
            more_lines_begin = more_lines_begin + more_lines_height + 13
            more_lines_height = -1 * more_lines_height - 13
        }

        // указатель но новую ноту
        let new_target: string
        if(this.pointer_note == '1') new_target = '2'
        else new_target = '1'

        // конфигурация новой ноты
        $('#note'+new_target+' .note').css({top: top}) // поставить на нужную линейку
        $('#note'+new_target+' .lines').css({
            top: more_lines_begin,
            height: more_lines_height
        }).children().first().css({top: -1 * more_lines_begin})

        // позиция подсказки
        let octave = zero - 104 + 1 + 26 - note.octave * 91

        // анимация смены нот
        $('#note'+this.pointer_note).animate({left: -72}, 80, function(){ $(this).css({left: 387 + 'px'}) }) // анимация удаления ноты
        $('#note'+new_target).animate({left: 213}, 80) // анимация появления ноты
        if(this.state.help) $('#octave').animate({top: octave}, 80)
            else $('#octave').css({top: octave + 'px'})

        // переставить указатель на текущую ноту
        this.pointer_note = new_target
    }
}

export class ControlElements extends NNGI.Widget<App> {
    draw(target: JQuery<HTMLElement>) {
        this.state.control = this
        let element = $('<div>', {id: 'control_elements', append: [
            listNotes(),
            listOctave(),
            showResponse(),
            this.showInput(),
            showMessage(),
            this.showNext(),
            $('<div>', {id: 'options', append: [
                this.showPlayOption(),
                this.showHelpOption()
            ]})
        ]})
        target.append(element)
        return {it: element, container: element}
    }

    resetMessage() {
        if(this.state.help) this.setMessage(this.state.note.name, 2)
        else this.setMessage('', 0)
    }

    updateScore() {
        $('#response .all').text(this.state.all)
        $('#response .correct').text(this.state.correct)
    }

    // type: 0 - ok, 1 - error, 2 - help
    setMessage(text: string, type: number) {
        $('#message').text(text)
        $('#message').attr('class', '')
        if(type == 0) $('#message').addClass('ok')
        else if(type == 1) $('#message').addClass('error')
        else $('#message').addClass('help')
    }

    inputReset() {
        $('#in').val('').removeAttr('disabled').focus()
    }

    inputOff() {
        $('#in').attr('disabled', 'disabled')
    }

    next() {
        this.inputOff()
        this.updateScore()
        this.state.next_step = true
        if(this.state.play)
            this.state.timeId =
                setTimeout(function(){ this.state.start() }.bind(this), 1000)
    }

    showInput(): JQuery<HTMLElement> {
        let element = $('<input>', {type: 'text', id: 'in', class: 'off', disabled: 'disabled'})
        element.on('input', function(e){
            let t = $(e.target).val()
            t = (<string>t).toUpperCase()
            t = t.replace(' ', '')
            $('#in').val(t)

            let n = t.length
            if(t == this.state.note.letter) {
                this.state.all++
                this.state.correct++
                this.setMessage(this.state.note.name, 0)
                this.state.sound.play(this.state.note.letter)
                this.next()
            } else if(this.state.note.letter.substr(0,n) !== t) {
                this.state.all++ // error
                this.setMessage(this.state.note.name, 1)
                this.state.soundError.play()
                this.next()
            }
        }.bind(this))
        return $('<div>', {id: 'input', append: element})
    }

    showNext(): JQuery<HTMLElement> {
        let element = $('<div>', {id: 'next', append: [
            $('<div>', {class: 'button'}).html('Дальше (<span>Space</span>)')
        ]})
        element.on('click', function(){
            if(this.state.next_step) {
                this.state.start()
            }
        }.bind(this))
        return element
    }

    showPlayOption(): JQuery<HTMLElement> {
        let element = $('<div>', {id: 'play_option', class: 'item'}).html('<span class="text">Автоматом следующую: <span class="flag">Off</span></span>')
        element.on('click', function(e){
            if(this.state.play) {
                this.state.play = false
                clearTimeout(this.state.timeId)
                $('.flag', e.target).text('Off')
            } else {
                this.state.play = true
                $('.flag', e.target).text('On')
                if(this.state.next_step) this.state.start()
            }
        }.bind(this))
        return element
    }

    showHelpOption(): JQuery<HTMLElement> {
        let element = $('<div>', {id: 'show_help', class: 'item'}).html('<span class="text">Показать помощь: <span class="flag">Off</span></span>')
        element.on('click', function(e){
            if(this.state.help) {
                this.state.help = false
                $('.flag', e.target).text('Off')
            } else {
                this.state.help = true
                $('.flag', e.target).text('On')
            }
            this.state.noteView.renderHelp()
        }.bind(this))
        return element
    }
}

function showResponse(): JQuery<HTMLElement> {
    let element = $('<div>', {id: 'response'}).html('Ответы: <span class="correct">0</span> из <span class="all">0</span>')
    return element
}

function showMessage(): JQuery<HTMLElement> {
    let element = $('<div>', {id: 'message', text: ''})
    return element
}

function listNotes(): JQuery<HTMLElement> {
    let ul = $('<ul>', {id: 'notes'}).html(
        '<li class="title">Ноты:</li>' +
        '<li><span>C</span> - До</li>' +
        '<li><span>D</span> - Ре</li>' +
        '<li><span>E</span> - Ми</li>' +
        '<li><span>F</span> - Фа</li>' +
        '<li><span>G</span> - Соль</li>' +
        '<li><span>A</span> - Ля</li>' +
        '<li><span>B</span> - Си</li>'
    )
    return ul
}

function listOctave(): JQuery<HTMLElement> {
    let ul = $('<ul>', {id: 'octaves'}).html(
        '<li class="title">Октавы:</li>' +
        '<li><span>C</span>ontra - Контр</li>' +
        '<li><span>G</span>reat - Большая</li>' +
        '<li><span>S</span>mal - Малая</li>' +
        '<li><span>1</span> Line - Первая</li>' +
        '<li><span>2</span> Line - Вторая</li>' +
        '<li><span>3</span> Line - Третья</li>'
    )
    return ul
}