///<reference path="../node_modules/@types/jquery/index.d.ts" />
/// <reference path="./howler.d.ts" />

import * as NNGI from "./NNGI"
import * as App from "./App"

let widget: App.Main<App.App> = new App.Main
widget.children.push(new App.NoteLines)
widget.children.push(new App.ControlElements)

let app = new App.App

$(document).ready(function() {
    widget.build(app, undefined, $('body'))

    $(window).resize( () =>  widget.eventWidgetUp(new NNGI.Resize))

    $(document).keydown((e) => {
        if(e.which == 32 && app.next_step) {
            app.start()
        }
    })

    app.start()
})