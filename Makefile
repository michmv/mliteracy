all: resources/main.js resources/style.css

resources/main.js: src/main.ts src/App.ts src/NNGI.ts src/howler.d.ts
	tsc --module amd src/main.ts --outFile resources/main.js -sourcemap

resources/style.css: src/style.less
	lessc src/style.less > resources/style.css

clean:
	rm -rf resources/main.js
	rm -rf resources/main.js.map
	rm -rf resources/style.css