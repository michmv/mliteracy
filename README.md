# Music literacy
Браузерное приложение для изучение музыкальных нот скрипичного и басового ключа. [Скачать тут](https://bitbucket.org/michmv/mliteracy/src/release/).

[Demo](http://mmv-module.herokuapp.com/demos/mliteracy/)

![Preview](./preview.jpg)

## Компиляция
Вам надо установить следующие утилиты:

* [typescript](https://www.typescriptlang.org/)
* [lessc](http://lesscss.org/)
* GNU Make

```sh
$ git clone https://bitbucket.org/michmv/mliteracy.git
$ cd mliteracy
$ make
```


## Использовать
Открыть в браузере `index.html` перетащив файл в браузер или нажав Ctrl+O.
## Лицензия
MIT
